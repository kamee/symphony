import Euterpea

bass = times 3 (a 6 qn :+: a 1 qn)

layer_1 = times 3 (d 6 qn :+: d 1 qn)

sound1 = instrument TaikoDrum(bass) :=: instrument ElectricBassFingered(layer_1)

background_melody = instrument HammondOrgan(times 4 (as 2 dqn :+: cs 2 en :+: b 3 en :+: gs 2 en :+: gs 2 en))

melody = instrument TaikoDrum(bass) :+: instrument ElectricBassFingered(layer_1) :=: instrument TaikoDrum(bass) :+: instrument TaikoDrum(bass) :+: sound1 :+: times 3(instrument TaikoDrum(layer_1)) :=: background_melody

main :: IO()
main = playDev 2 $ melody
-- main = writeMidi "main.mid" melody
