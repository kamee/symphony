import Euterpea

intro = as 0 en :+: gs 0 sn :+: bs 1 en :+: ds 1 sn

intro_1 = gs 1 sn :+: gs 0 en :+: gs 1 sn :+: b 0 en :+: ds 1 sn :+: e 1 en

layer_1 = Modify (Tempo 0.75) (Modify (Instrument SynthBass2) intro)
layer_1_ = Modify (Tempo 0.9) (Modify (Instrument SynthBass2) intro)

layer_2 = Modify (Tempo 0.75) (Modify (Instrument SynthBass2) intro_1)

melody = times 20(layer_1) :+: times 2(layer_1_) :+: times 10(layer_2) :+: times 5(layer_1_)

main :: IO()
main = playDev 2 $ melody
-- main = writeMidi "demo12.mid" melody
