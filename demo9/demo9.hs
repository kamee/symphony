import Euterpea

intro = times 3 (c 2 qn :+: b 2 qn :+: g 2 qn :+: e 2 qn) :+: times 2 (c 2 qn :+: b 2 qn :+: c 2 qn :+: b 2 qn) :+: times 4 (g 2 qn :+: e 2 qn :+: a 2 qn :+: f 2 qn) :+: times 2 (f 2 qn) :+: times 3(f 2 dqn :+: f 2 en :+: a 2 en :+: a 2 dqn)

sound1 = times 2 (f 2 qn) :+: times 3(f 2 dqn :+: f 2 en :+: a 2 en :+: a 2 dqn)

melody = instrument FX4Atmosphere(intro) :+: instrument Pad3Polysynth(sound1)

main :: IO()
-- main = playDev 2 $ melody
main = writeMidi "demo9.mid" melody
