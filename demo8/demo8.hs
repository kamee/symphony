import Euterpea

intro = times 2 (b 3 hn :+: rest qn :+: d 3 en :+: e 3 dwn :+: f 3 dwn)

back = tempo (6/9) $ times 2 (b 4 en :+: d 4 en :+: e 4 en :+: f 4 en)

sound1 = instrument Vibraphone(intro) :+: instrument ElectricGrandPiano(intro) :=: instrument Marimba(back)

main :: IO()
main = playDev 2 $ sound1
-- main = writeMidi "demo8.mid" sound1
