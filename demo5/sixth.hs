import Euterpea

introduction = c 0 qn :+: b 0 qn :+: g 0 qn :+: e 0 qn :+: c 0 qn

introduction_2 = cs 0 qn :+: bs 0 qn :+: gs 0 qn :+: es 0 qn :+: cs 0 qn

introduction_3 = cf 0 qn :+: bf 0 qn :+: gf 0 qn :+: ef 0 qn :+: cf 0 qn

percussion = times 6 $ perc BassDrum1 dqn :+: perc AcousticSnare dqn :+: perc ElectricSnare dqn

perc1 = times 2 $ perc ElectricSnare qn

__percussion = times 6 $ perc Maracas dqn

melody = instrument Xylophone(times 2(introduction :+: introduction_2)) :+: instrument Xylophone(introduction :+: introduction_2 :+: introduction_3) :=: percussion :+: times 5(instrument AcousticBass(introduction_3)) :=: percussion :+: perc1 :=: __percussion

main :: IO()
-- main = playDev 2 $ melody
main = writeMidi "sixth.mid" melody
