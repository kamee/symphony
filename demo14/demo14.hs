import Euterpea

intro = times 3(a 4 en :+: a 1 en :+: e 4 en :+: e 1 en :+: a 1 en :+: e 4 en :+: a 1 en :+: a 4 en)

intro_2 = times 3(e 1 en :+: e 4 en :+: f 1 en :+: f 4 en :+: f 1 en :+: e 1 en :+: f 1 en :+: e 4 en)

perc_ = times 3 $ perc AcousticBassDrum qn :+: rest en :+: perc BassDrum1 en :+: perc AcousticSnare en :+: perc BassDrum1 en

melody = instrument AcousticBass(intro) :+: instrument AcousticBass(intro) :=: perc_ :+: instrument AcousticBass(intro_2) :=: perc_ 

main :: IO()
-- main = playDev 2 $ melody
main = writeMidi "demo14.mid" melody
