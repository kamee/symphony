import Euterpea
import Euterpea.IO.MIDI.ExportMidiFile

introduction = d 2 qn :+: f 4 qn :+: a 4 qn :+: g 4 qn :+: f 4 qn :+: e 4 qn :+: d 4 qn :+: f 4 qn :+: a 4 qn :+: g 4 qn :+: f 4 qn :+: e 4 qn :+: d 4 qn :+: d 4 qn :+: d 4 qn :+: d 2 qn :+: f 4 qn :+: a 4 qn :+: g 4 qn :+: f 4 qn :+: e 4 qn :+: d 4 qn :+: f 4 qn

introduction_cont = times 3(f 4 en :+: d 4 qn :+: f 4 en)

layer_1 = times 2(f 3 en :+: rest qn :+: d 3 en :+: rest qn :+: f 3 en) :+: times 3 (f 3 en :+: d 3 en :+: f 3 en)

layer_2 = times 4 (f 3 en :+: d 3 en :+: f 3 en)

layer_3 = times 4(d 2 qn :+: f 2 qn :+: a 4 qn)

layer_4 = d 2 qn :+: d 2 den :+: times 2(d 2 en :+: f 2 en :+: a 4 en)

layer_5 = times 3(d 2 en :+: f 2 en :+: a 2 en :+: g 2 en :+: f 2 en :+: d 2 en)

layer_6 = c 2 qn :+: e 2 qn :+: g 2 qn :+: e 2 qn :+: g 2 qn :+: b 2 qn

layer_7 = times 2(a 1 qn :+: rest qn :+: b 1 qn :+: cs 1 qn :+: as 1 qn :+: bs 1 qn)

interruption = times 2 (a 1 qn :+: rest hn)

smoothing = times 2(e 4 qn :+: g 4 qn :+: b 4 qn)

layer_8 = times 2 (e 4 qn :+: g 4 qn :+: b 4 qn :+: d 4 qn :+: f 4 qn :+: a 4 qn :+: c 4 qn)

layer_9 = d 3 hn :+: f 3 dhn :+: a 3 hn :+: g 3 hn :+: f 3 hn :+: e 3 dhn :+: e 2 dqn :+: f 3 qn

continuation = times 2(c 1 hn :+: e 1 hn :+: g 1 hn :+: e 1 hn :+: g 1 hn :+: b 1 hn :+: g 1 dhn :+: b 1 dhn)

ending = times 2 (g 4 qn :+: e 4 qn :+: f 4 qn :+: d 4 qn)

melody_begin = instrument FX8SciFi(introduction :+: introduction_cont) :+: instrument FX8SciFi(introduction_cont) :=: times 2(instrument AcousticBass(layer_1))

melody_cont = instrument AcousticBass(layer_2) :=: instrument SynthBass1(layer_3) :+: instrument SynthBass1(layer_4) :+: instrument SynthBass1(layer_5)

melody_repeat = times 2 (instrument SynthBass1(layer_6) :=: instrument AcousticBass(layer_5)) :=: instrument SynthBass2(layer_7)

melody_interruption = instrument ReverseCymbal(interruption)

smoothing_effect = instrument SynthBass1(smoothing) :=: instrument AcousticBass(layer_8)

melody = melody_begin :+: melody_cont :+: melody_repeat :+: melody_interruption :=: smoothing_effect :+: instrument SynthBass1(layer_9) :+: instrument SynthBass1(continuation) :+: instrument SynthBass2(continuation) :+: instrument AcousticBass(ending)


main :: IO()
main = playDev 2 $ melody
-- main = writeMidi "first.mid" melody
