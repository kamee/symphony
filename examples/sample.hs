import Euterpea

-- Using Music Pitch

melody :: Music Pitch
melody = line [c 5 qn, c 5 qn, g 5 qn, 
     g 5 qn, a 5 qn, a 5 qn, g 5 hn]

chords :: Music Pitch
chords = chord [c 3 wn, e 3 wn, g 3 wn] :+:
    chord [c 3 hn, f 3 hn, a 3 hn] :+:
    chord [e 3 hn, g 3 hn, c 4 hn]

twinkle = melody :=: chords

--Using Music AbsPitch and Music (AbsPitch, Volume) to
--create a major scale.

ps :: [AbsPitch]
ps = [60,62,64,65,67,69,71,72]

majScale :: Music AbsPitch
majScale = line (map (note en) ps)

vols :: [Volume]
vols = [40,50,60,70,80,90,100,110]

majScale2 :: Music (AbsPitch, Volume)
majScale2 = line (map (note en) (zip ps vols))

main :: IO()
main = playDev 2 $ chords
