import Euterpea

-- understand roll

funkGroove = let p1 = perc LowTom qn
		 p2 = perc AcousticSnare en
	     in tempo 3 $ instrument Percussion $ cut 8
		(p1 :+: qnr :+: p2 :+: qnr :+: p2 :+: p1 :+: p1 :+: qnr :+: p2 :+: enr)
		-- :=: roll en (perc ClosedHitHat 2))

main :: IO()
main = playDev 2 $ funkGroove
