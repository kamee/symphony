import Euterpea

pr1, pr2 :: Pitch -> Music Pitch
pr1 p = tempo (5/6) (tempo (4/3) (mkLn 1 p qn :+: tempo (3/2) (mkLn 3 p en :+: mkLn 2 p sn :+: mkLn 1 p qn) :+: mkLn 1 p qn) :+: tempo (3/2) (mkLn 6 p en))

pr2 p =
	let m1 = tempo (5 / 4) (tempo (3/2) m2 :+: m2)
	    m2 = mkLn 3 p en
	in tempo (7/6) (m1 :+:
			tempo(5/4) (mkLn 5 p en) :+:
			m1 :+:
			tempo (3/2) m2)
mkLn n p d = line $ take n $ repeat $ note d p

pr12 :: Music Pitch
pr12 = pr1 (C, 4) :=: pr2 (G, 4) :+: pr2 (D, 4) :+: pr1 (A, 2)

main :: IO()
main = playDev 2 $ pr12
