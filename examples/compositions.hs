import Euterpea

wts :: Pitch -> [Music Pitch]
wts p = let f ap = note (1/32) (pitch (absPitch p + ap))
        in map f [0, 2, 4, 6, 8, 10]

main :: IO()
main = playDev 2 $
