-- https://github.com/cowgirl-coding/euterpea-sketches/blob/master/src/Composition/Percussion.hs

import Euterpea

beat :: Music Pitch
beat =
  let r = replicate
      rhythm = (r 4 sn) ++ 
               (r 2 ((en/3))) ++
               (r 1 ((en/3) * 2)) ++
               (r 2 (en/3)) ++
               (r 2 sn) ++
               (r 4 tn) ++
               (r 5 (en/5)) ++
               (r 2 sn) 
  in line $ map (perc LowWoodBlock) rhythm
  
main :: IO()
main = playDev 2 $ beat
