Tests of the MIDI interface.

> loadMidiFile fn = do
>   r <- importFile fn 
>   case r of
>     Left err -> error err
>     Right m  -> return m

Music into a MIDI file.

> tab m = do
>           exportFile "test.mid" $ makeMidi (m, defCon, defUpm)

Music to a MidiFile datatype and back to Music.

> tad m = fromMidi (testMidi m)

A MIDI file to a MidiFile datatype and back to a MIDI file.

> tcb file = do
>              x <- loadMidiFile file
>              exportFile "test.mid" x

MIDI file to MidiFile datatype.

> tc file = do
>             x <- loadMidiFile file
>             print x

MIDI file to Music, a UserPatchMap, and a Context.

> tcd file = do
>              x <- loadMidiFile file
>              print $ fst3 $ fromMidi x
>              print $ snd3 $ fromMidi x
>              print $ thd3 $ fromMidi x

A MIDI file to Music and back to a MIDI file.

> tcdab file = do
>              x <- loadMidiFile file
>              exportFile "test.mid" $ makeMidi $ fromMidi x

> fst3 (a,b,c) = a
> snd3 (a,b,c) = b
> thd3 (a,b,c) = c

--------------------------------------------------
--------------------------------------------------
--------------------------------------------------
--------------------------------------------------


Tests of the MIDI interface.

> loadMidiFile fn = do
>   r <- importFile fn 
>   case r of
>     Left err -> error err
>     Right m  -> return m

Music into a MIDI file.

> tab m = writeMidi m

Music to a Midi datatype and back to Music.

> tad m = fromMidi $ toMidi $ perform m

A MIDI file to a MidiFile datatype and back to a MIDI file.

> tcb file = do
>              x <- loadMidiFile file
>              exportFile "test.mid" x

MIDI file to MidiFile datatype.

> tc file = do
>             x <- loadMidiFile file
>             print x

MIDI file to Music, a UserPatchMap, and a Context.

> tcd file = do
>              x <- loadMidiFile file
>              print $ fromMidi x

A MIDI file to Music and back to a MIDI file.

> tcdab file = do
>              x <- loadMidiFile file
>              exportFile "test.mid" $ toMidi $ perform $ fromMidi x
