import Euterpea
-- import System.Random

{-
randInts :: Int -> [Int]
randInts seed = recInts (mkStdGen seed) where
    recInts g = let (i,g') = next g in i : recInts g'

randIntsRange :: (Int, Int) -> Int -> [Int]
randIntsRange (lower, upper) = 
    map (\i -> (i `mod` (upper-lower)) + lower) . randInts 
-}

{-
melGen :: Int -> Music (Pitch, Volume)
melGen s = 
     let pitches = map pitch $ 43 s
         vols = (s+1)
     in  line $ map (note sn) $ zip pitches vols

infinitePlay = playDev 2 $ melGen 42
-}

--Finally we use this function to create three lines in parallel,
--each affected by some Control options. To make this piece finite, 
--we will use Euterpea's "cut" function to take a fixed number of
--measures from the otherwise infinite series that melGen produces.
--Various Modify constructors are used to alter the performance of 
--each instrument's part: speeding up (Accelerando), slowing down 
--(Ritardando), and getting louder (Crescendo). The tempo (Tmp) 
--modifiers result in interesting rhythmic textures, such that the 
--voices do not play in lock-step.

somethingWeird = 
    let part1 = instrument Xylophone $ dim $ rit $ cut 6 $ melGen 345
        part2 = instrument Marimba $ cut 4 $ melGen 234
        part3 = instrument TubularBells $ cre $ acc $ cut 8 $ melGen 789
    in  chord [part1, part2, part3] where
    rit = Modify (Phrase [Tmp $ Ritardando 0.5])
    acc = Modify (Phrase [Tmp $ Accelerando 0.5])
    dim = Modify (Phrase [Dyn $ Diminuendo 0.5])
    cre = Modify (Phrase [Dyn $ Crescendo 0.5])

main :: IO()
main = playDev 2 $ somethingWeird
