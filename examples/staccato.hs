import Euterpea

cMajScale = Modify (Tempo 2)
            (line [c 4 en, d 4 en, e 4 en, f 4 en, 
                   g 4 en, a 4 en, b 4 en, c 5 en])

cms' = line [c 4 en, d 4 en, e 4 en, f 4 en, 
             g 4 en, a 4 en, b 4 en, c 5 en]

cms = cMajScale

t1 = (Modify (Instrument Percussion)
       (Modify (Phrase [Art (Staccato (1/10))]) cms :+:
        cms                             :+:
        Modify (Phrase [Art (Legato  (11/10))]) cms    ))

temp = Modify (Instrument AcousticGrandPiano) 
         (Modify (Phrase [Dyn (Crescendo 4)]) (c 4 en))

mu2 = Modify (Instrument Vibraphone)
       (Modify (Phrase [Dyn (Diminuendo (3/4))]) cms :+:
         (Modify (Phrase [Dyn (Crescendo 4), Dyn (Loudness 25)]) cms))

t3 = (Modify (Instrument Flute) 
       (Modify (Phrase [Tmp (Accelerando 0.3)]) cms :+:
        Modify (Phrase [Tmp (Ritardando  0.6)]) cms    ))

test = t1 :+: rest qn :+: temp :+: rest qn :+: mu2 :+: rest qn :+: t3

main :: IO()
main = do playDev 2 $ test 
