t251' :: Music Pitch
t251' =
let dMinor7 = d 3 hn :=: c 4 hn :=: c 5 hn :=:
f 5 hn :=: c 6 hn
gDom7 = g 3 hn :=: f 4 hn :=: b 4 hn :=:
f 5 hn :=: b 5 hn
cMajor7 = c 3 hn :=: b 3 hn :=: b 4 hn :=:
e 5 hn :=: b 5 hn
in dMinor7 :+: gDom7 :+: cMajor7

### 

canonInD = tempo (90/120) (mainVoice :=: bassLine)
bassPhrase :: Music Pitch
bassPhrase = line . fmap ($ hn) $
[d 3, a 2, b 2, fs 2, g 2, d 2, g 2, a 2]
bassLine :: Music Pitch
bassLine = times 5 bassPhrase -- & keysig D Major
mainVoice :: Music Pitch
mainVoice = line [phrase0, phrase1, phrase2
,phrase3, phrase4] -- & keysig D Major
where
phrase0 = rest (dur bassPhrase)
phrase1 = line . fmap ($ hn) $
[fs 5, e 5, d 5, cs 5, b 4, a 4, b 4, cs 5]
phrase2 = phrase1 :=: (fmap ($ hn)
[d 5, cs 5, b 4, a 4, g 4, fs 4, g 4, a 4] & line)
phrase3 = phrase2 :=: (fmap (\n -> rest qn :+: n qn)
[a 4, a 4, fs 4, fs 4, d 4, d 4, d 4, g 4] & line)
phrase4 = phrase2 :=: ((fmap (\(n0,n1) ->
line [rest en, n0 en, n1 en, n0 en, rest qn, n0 qn])
[(a 4, d 5),(fs 4, b 4), (d 4, g 4)] & line) :+:
line [rest en, d 4 en, g 4 en, d 4 en, rest qn, g 4 qn])
