import Euterpea

-- import trill

{-
ssfMel :: Music Pitch
ssfMel = line (l1 + l2 + l3 + l4)
	where l1 = [trilln 2 5 (bf 6 en), ef 7 en, ef 6 en, ef 7 en]
	      l2 = [bf 6 sn, c 7 sn, bf 6 sn, g 6 sn, ef 6 en, bf 5 en]
	      l3 = [ef 6 sn, f 6 sn, g 6 sn, af 6 sn, bf 6 en, ef 7 en]
	      l4 = [trill 2 tn (bf 6 qn), bf 6 sn, denr]

starsAndStripes :: Music Pitch
starsAndStripes = instrument Flute ssfMel

roll :: Dur -> Music Pitch -> Music Pitch
rolln :: Int -> Music Pitch -> Music Pitch
roll dur m = trill 0 dur m
rolln nTimes m = trilln 0 nTimes m
-}

t13note = Prim (Note qn (C,5))
--t13 =  play (trill   1 sn t13note)
--t13a = play (trill'  2 dqn t13note)
--t13b = play (trilln  1 5 t13note)
--t13c = play (trilln' 3 7 t13note)
--t13d = play (roll tn t13note)
--t13e = play (Modify (Tempo (2/3)) 
--               (Modify (Transpose 2) 
--                 (Modify (Instrument AcousticGrandPiano) 
--                   (trilln' 2 7 t13note))))

main :: IO()
-- main = playDev 2 $ 
main = playDev 2 $ trill 1 sn t13note
