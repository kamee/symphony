import Euterpea

e = Event 0 Cello 27 (1/4) 50 []

ss pat n tr te = transpose tr $ tempo te $ simToMusic $ fringe n $ selfSim pat

m3 :: [SNote]
m3 = [(hn, 3), (qn, 4), (qn, 0), (hn, 6)]
tm3 = ss m3 4 50 (1/4)
ttm3 = let l1 = instrument Flute tm3
	   l2 = instrument AcousticBass $ transpoase(-9) (revM tm3)
       in l1 := l2
m4 :: [SNote]
m4 = [(hn, 3), (hn, 8), (hn, 22), (qn, 4), (qn, 7), (qn, 21),
      (qn, 0), (qn, 5), (qn, 15), (wn, 6), (wn, 9), (wn, 19)]
tm4 = ss m4 3 50 8
