m1 :: Music Pitch
m1 = mkLIne1 (random Rs (30, 70) sGen)
m2 :: Music Pitch
m2 = let rs1 = rands linear sGen
     in mkLine1 (map toAbsP1 rs1)
-- exponential distribution
m3 :: Float -> Music Pitch
m3 lam = let rs1 = rands (exponential lam) sGen
         in mkLine1 (map toAbsP1 rs1)
-- Gaussian distribution
m4 :: Float -> Float -> Music Pitch
m4 sig mu = let rs1 = randss (gaussian sig mu) sGen
            in mkLine1 (map toAbsP1 rs1)
