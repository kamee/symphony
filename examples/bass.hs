import Euterpea

bass3 = b1 :+: b2 :+: b3 :+: b4 :+: b1

b1 = bass2 0 :+: rest wn
b2 = transpose 5 $ bass2 2 :+: rest wn
b3 = retro $ transpose 7 $ bass2 4 :+: rest wn
b4 = retro $ transpose 5 $ bass2 2 :+: rest wn        

---  :+: transpose 5 $ bass2 2 :+: bass2  0

bass2 d = instrument Bassoon $ bass1 :=: (instrument Tuba $ transpose 19 $ offset d $ retro $ bass1)

-- The bass line 
-- bassNote :: Dur -> Music Pitch
bass1= line $ [d 1 wn, f 1 wn, a 1 wn,  g 1 1, f 1 qn, e 1 qn, d 1 qn, c 1 qn, e 1 hn, d 1 wn]
-- bassLine = line $ [bassNote 6, rest 1, bassNote' 3, rest 3, bassNote'' 3, rest 2, bassNote''' 4, rest 1 , bassNote 7, rest 4]

main :: IO()
main = playDev 2 $ bass3 :+: rest hn :+: rest hn :+: bass1
