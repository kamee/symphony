import Euterpea

-- Complete first phrase of "Twinkle Twinkle Little Star"

twinkle :: Music Pitch
twinkle = line [c 5 qn, c 5 qn, g 5 qn, 
     g 5 qn, a 5 qn, a 5 qn, g 5 hn,
     f 4 qn, f 4 qn, e 4 qn, e 4 qn,
     d 4 qn, d 4 qn, c 4 hn]

-- Four repetitions of this melody:

twinkle4 = line [twinkle, twinkle, twinkle, twinkle]

-- A round using three instruments:

twinkleRound = 
     instrument Marimba twinkle4 :=:
     instrument Vibraphone (rest wn :+: twinkle4) :=:
     instrument Celesta (rest 2 :+: twinkle4)

-- A simple phase composition. Phase compositions use simple 
-- motifs repeated at very slighly different tempos to create 
-- interesting rhythmic patterns over time.

m1, m2, m3, phase :: Music Pitch
m1 = c 4 en :+: rest en :+: d 4 en :+: rest en :+: m1
m2 = Modify (Transpose 3) (Modify (Tempo 1.01) m1)
m3 = Modify (Transpose 5) (Modify (Tempo 1.02) m1)
phase = m1 :=: m2 :=: m3

main :: IO()
main = playDev 2 $ twinkleRound :+: rest qn :+: phase
