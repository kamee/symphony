import Euterpea

beat :: Music Pitch
beat =
  let r = replicate
      rhythm = (r 4 sn) ++
               (r 2 ((en/3))) ++
               (r 1 ((en/3) * 2)) ++
               (r 2 (en/3)) ++
               (r 2 sn) ++
               (r 4 tn) ++
               (r 5 (en/5)) ++
               (r 2 sn)
  in line $ map (perc LowWoodBlock) rhythm

sound1 = instrument HonkyTonkPiano(c 3 qn :+: g 3 sn :+: d 3 sn :+: c 2 en)

sound2 = instrument HonkyTonkPiano(g 2 sn :+: d 3 sn :+: e 2 en :+: c 2 en)

mel = beat :+: times 3(sound1) :+: times 3(sound2) :+: times 3(sound1)

main :: IO()
-- main = playDev 2 $ mel
main = writeMidi "demo13.mid" mel
