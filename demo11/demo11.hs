import Euterpea

intro_1 = times 3 (c 6 wn :+: c 1 qn)

bass = times 3 (a 6 qn :+: a 1 qn)

smth = times 3 (d 6 qn :+: d 1 qn)

intro_2 = times 2 (c 1 qn :+: c 2 qn)

sound = times 3 (d 2 qn :+: d 3 qn :+: times 3(d 3 en :+: e 2 en :+: e 3 en :+: d 3 en :+: a 3 en :+: a 3 den :+: f 3 en))

melody = instrument FX7Echoes(intro_1) :+: instrument TaikoDrum(bass) :+: instrument TaikoDrum(bass) :=: instrument ElectricBassFingered(smth) :=: instrument FX7Echoes(intro_2) :+: instrument SynthBass1(sound)

main :: IO()
main = playDev 2 $ melody
-- main = writeMidi "demo11.mid" melody
