import Euterpea

introduction__ = c 0 en :+: b 0 en :+: g 0 en :+: e 0 en :+: c 0 en

introduction__1 = cs 0 en :+: bs 0 en :+: gs 0 en :+: es 0 en :+: cs 0 en

introduction__2 = cf 0 en :+: bf 0 en :+: gf 0 en :+: ef 0 en :+: cf 0 en

introduction_1 = css 0 en :+: bss 0 en :+: gss 0 en :+: ess 0 en :+: css 0 en

introduction_2 = cff 0 en :+: bff 0 en :+: gff 0 en :+: eff 0 en :+: cff 0 en

test = css 1 en :+: bss 1 en :+: gss 1 en :+: ess 1 en :+: css 1 en

to_test = ass 1 qn :+: fss 1 qn :+: gss 1 qn :+: css 1 qn

bass = times 4 $ perc AcousticBassDrum qn

bass_1 = times 2 $ perc AcousticBassDrum en

bass_2 = times 2 $ perc BassDrum1 en

melody = instrument AcousticGrandPiano(introduction__ :+: introduction__1 :+: introduction__2) :+: instrument AcousticGrandPiano (introduction_1 :=: bass :+: rest qn :+: introduction_2 :=: bass_1)

melody_2 = times 3 (instrument AcousticGrandPiano(test) :+: bass_2 :+: instrument AcousticGrandPiano (to_test) :+: bass_2)

main :: IO()
-- main = writeMidi "third.mid" melody_2
main = playDev 2 $ melody_2
