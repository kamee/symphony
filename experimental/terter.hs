import Euterpea

{-
sound1 = times 2(a 2 en :+: b 2 en :+: d 2 en)

mel_ = instrument  StringEnsemble1(sound1)

mel2 = instrument OrchestraHit(a 2 en :+: b 2 en)

mel = mel_ :+: mel2 :+: mel_ :+: mel_ :+: mel_ :+: mel2
-}

mel2 = instrument OrchestraHit(a 2 en :+: b 2 en)

sound1 = a 2 dwn :+: rest qn :+: bf 2 hn

sound2 = af 2 hn :+: rest qn :+: bf 2 dwn

back = instrument ChoirAahs(bf 2 hn)

-- mel = instrument StringEnsemble1(sound1 :+: rest qn :+: sound2 :+: mel2 :+: rest dwn :+: back :+: mel2)

-- sound3 = af 2 hn :=: instrument VoiceOohs(bf 2 hn) :+: bf 2 hn :+: instrument VoiceOohs(bf 2 hn)

-- sound3 = g 3 hn :+: rest qn :+: f 3 en :+: f 3 dwn

sound3 = times 2 (b 3 hn :+: rest qn :+: d 3 en :+: e 3 dwn :+: f 3 dwn)

mel = instrument StringEnsemble2(sound3)

main :: IO()
main = playDev 2 $ mel
