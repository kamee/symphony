import Euterpea

mel2 = instrument OrchestraHit(a 2 en :+: b 2 en)

sound1 = a 2 dwn :+: rest qn :+: bf 2 hn

sound2 = af 2 hn :+: rest qn :+: bf 2 dwn

back = instrument ChoirAahs(bf 2 hn)

sound3 = times 2 (b 3 hn :+: rest qn :+: d 3 en :+: e 3 dwn :+: f 3 dwn)

bassDrumPhrase = times 16 $ perc AcousticBassDrum qn

sound0 = af 3 hn :+: df 3 dwn :+: f 3 dwn :+: g 3 dwn

_m = instrument AcousticGrandPiano(sound1 :+: rest qn :+: sound2 :+: mel2 :+: rest dwn :+: back :+: mel2)

mel = _m :=: bassDrumPhrase

-- mel = instrument StringEnsemble2(sound3) :=: _m

main :: IO()
main = playDev 2 $ mel
