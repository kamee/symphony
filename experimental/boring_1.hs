import Euterpea

introduction = c 0 qn :+: b 0 qn :+: g 0 qn :+: e 0 qn :+: c 0 qn :+: cs 0 qn :+: bs 0 qn :+: gs 0 qn :+: es 0 qn :+: cs 0 qn :+: cf 0 qn :+: bf 0 qn :+: gf 0 qn :+: ef 0 qn :+: cf 0 qn

quick_introduction = cff 0 en :+: bff 0 en :+: gff 0 en :+: eff 0 en :+: cff 0 en :+: css 0 en :+: bss 0 en :+: gss 0 en :+: ess 0 en :+: css 0 en

intro_sound = instrument Xylophone(introduction)

quick_intro_sound = instrument Xylophone(times 3(quick_introduction))

percussion = times 3 $ perc BassDrum1 dqn :+: perc AcousticSnare dqn :+: perc ElectricSnare dqn

melody = intro_sound :+: (intro_sound :=: percussion :+: quick_intro_sound :=: percussion)

-- test = a 0 qn :+: f 0 qn :+: g 0 qn :+: c 0 qn

-- c e g 
-- g b d

test = c 0 qn :+: e 0 qn :+: g 0 qn :+: b 0 qn :+: d 0 qn

-- testing = instrument Xylophone(test) :+: 

-- testing = instrument RhodesPiano(test) 

-- testing = instrument AcousticGuitarNylon(test)

main :: IO()
main = playDev 2 $ melody
-- main = playDev 2 $ testing
