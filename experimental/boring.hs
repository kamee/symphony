import Euterpea

-- TODO: use volume for different parts in this music
-- see example examples/volume.hs

introduction = c 2 qn :+: b 2 qn :+: g 2 qn :+: e 2 qn :+: c 2 qn :+: cs 2 den :+: bs 2 qn :+: gs 2 qn :+: es 2 qn :+: cs 2 qn :+: cf 2 qn :+: bf 2 qn :+: gf 2 qn :+: ef 2 qn :+: cf 2 qn

cont = cf 2 qn :+: ef 2 qn

cont_2 = cf 2 qn :+: c 2 qn :+: e 2 qn :+: g 2 qn :+: b 2 qn :+: d 2 qn

cont_2_quick = times 2(cf 2 den :+: e 2 en)

cont_3 = d 2 den :+: b 2 en :+: d 2 den :+: b 2 den :+: d 2 en :+: b 2 en :+: b 2 en :+: b 2 en :+: d 2 en

intro_part = c 2 qn :+: b 2 qn :+: g 2 qn

progress = bss 3 dqn :+: dss 3 dqn :+: gss 3 dqn :+: ess 3 dqn :+: css 3 dqn :+: css 3 dqn

quick_intro = c 2 en :+: b 2 den :+: g 2 den :+: e 2 en :+: c 2 en :+: cs 2 den :+: bs 2 den :+: gs 2 den :+: es 2 en :+: cs 2 en :+: cf 2 den :+: bf 2 den :+: gf 2 den :+: ef 2 en :+: cf 2 en

intro_sound = instrument SynthBass1(introduction)

cont_sound = instrument SynthBass1(cont)

cont_2_sound = instrument SynthBass1(cont_2)

cont_2_quick_sound = instrument SynthBass1(cont_2_quick)

cont_3_sound = instrument SynthBass1(cont_3)

intro_part_sound = instrument SynthBass1(intro_part)

quick_intro_sound = instrument SynthBass1(quick_intro)

progress_sound = instrument Pad4Choir(progress) :=: instrument SynthBass1(progress) :+: rest en :+: instrument SynthBass1(progress) :=: instrument Pad4Choir(progress)

percussion = times 7 $ perc BassDrum1 dqn :+: perc AcousticSnare dqn :+: perc ElectricSnare dqn

percussion_1 = times 8 $ perc BassDrum1 den :+: perc AcousticSnare den :+: perc ElectricSnare den

-- somewhere along this parts should be percussion
part_1 = intro_sound :+: (intro_sound) :+: (cont_sound :+: cont_2_sound :+: intro_part_sound :+: cont_2_sound :+: cont_2_quick_sound :+: cont_3_sound)

part_2 = progress_sound :=: percussion :=: progress_sound :+: rest hn :=: percussion

part_3 = intro_sound :=: percussion_1

melody = part_1 :+: part_1 :=: part_2 :+: part_3 :+: quick_intro_sound :=: percussion_1

main :: IO()
main = playDev 2 $ melody
