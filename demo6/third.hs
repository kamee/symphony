import Euterpea

intro = f 2 dqn :+: rest qn :+: g 2 dqn :+: rest dqn :+: e 2 dqn :+: g 2 dqn :+: rest sn

continuation = g 2 dqn :+: d 2 dqn :+: rest en :+: e 2 dqn :+: cf 2 dqn :+: rest en

mel = instrument HonkyTonkPiano(intro)

back = instrument Vibraphone(intro)

melody = times 2 (intro :+: continuation :+: mel :=: back)

main :: IO()
-- main = playDev 2 $ melody
main = writeMidi "third.mid" melody
