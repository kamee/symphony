import Euterpea

base = times 2(c 3 en :+: fs 3 en) :+: times 2 (c 3 en :+: fs 3 en :+: d 3 en :+: a 3 en :+: a 2 en :+: f 3 en) :+: times 3(d 3 en :+: e 2 en :+: e 3 en :+: d 3 en :+: a 3 en :+: a 3 den :+: f 3 en)

perc_ = times 3 $ perc AcousticBassDrum qn :+: rest en :+: perc BassDrum1 en :+: perc AcousticSnare en :+: perc BassDrum1 en

unknown = instrument SynthBass1(times 2 (a 3 qn :+: a 3 en :+: c 2 en :+: fs 2 en :+: fs 2 en :+: a 2 en :+: a 2 en :+: f 2 en))

sound1 = times 3(f 2 en :+: f 3 en :+: cs 3 en :+: cs 2 en :+: cs 2 den :+: d 2 en)

melody = instrument SynthBass1(base) :+: instrument SynthBass1(unknown) :=: perc_ :+:  times 3(instrument SynthBass1(unknown) :=: perc_ :=: instrument SynthBass2(sound1))

main :: IO()
-- main = playDev 2 $ melody
main = writeMidi "demo10.mid" melody
