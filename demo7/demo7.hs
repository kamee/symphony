import Euterpea

intro = times 10(g 2 en :+: e 2 en :+: c 2 en)

intro_ = times 5 (gs 0 en :+: ds 0 en)

cont = times 4(e 3 en :+: g 2 en :+:  g 2 en :+: f 3 en :+: e 2 en :+: e 3 en)

cont__ = times 5(e 2 en :+: e 3 en :+: es 3 en :+: e 2 en)

cont_ = times 5 (gs 0 en :+: ds 0 en :+: bs 0 en :+: as 0 en)

cont_2 = times 5(as 0 en :+: gs 0 sn :+: bs 1 en :+: ds 1 sn)

melody = instrument SynthBass2(intro) :+: instrument SynthBass2(intro) :=: instrument TaikoDrum(intro_) :+: instrument SynthBass2(intro) :=: instrument TaikoDrum(intro_) :+: instrument SynthBass2(cont) :+: instrument SynthBass2(cont) :=: instrument FX4Atmosphere(cont_) :+: instrument SynthBass2(cont__) :=: instrument FX4Atmosphere(cont_) :+: instrument SynthBass2(cont__) :=: instrument SynthBass2(cont_2)

main :: IO()
-- main = playDev 2 $ melody
main = writeMidi "demo7.mid" melody
