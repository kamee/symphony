import Euterpea

intro = times 2 (b 3 hn :+: rest qn :+: d 3 en :+: e 3 dwn :+: f 3 dwn)

back = tempo (6/9) $ times 4 (b 4 en :+: d 4 en :+: e 4 en :+: f 4 en)

cont = times 2 (a 4 en :+: d 4 en :+: e 4 en :+: b 4 en :+: cf 4 en)

sound1 = instrument Vibraphone(intro) :+: instrument Vibraphone(intro) :=: instrument Marimba(back) :+: instrument Vibraphone(cont) :=: sound3

sound3 = instrument SynthBass1 $ acc $ back where
	acc = Modify (Phrase [Dyn $ Diminuendo 0.5])

melody = sound1

main :: IO()
-- main = playDev 2 $ melody
main = writeMidi "first.mid" melody
